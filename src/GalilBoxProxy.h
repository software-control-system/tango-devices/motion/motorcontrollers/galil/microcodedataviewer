#ifndef _INCLUDE_GALILBOXPROXY_H
#define _INCLUDE_GALILBOXPROXY_H

#include <tango.h>
#include <DeviceProxyHelper.h>

#define uns32 unsigned int

class GalilBoxProxy {
public:
	GalilBoxProxy();
	~GalilBoxProxy();
	void init(string proxy_name,Tango::DeviceImpl* dev);
	double read_var(const std::string& var_name);
	void write_var(const std::string& var_name,double value);
	void start_process(const std::string& name,const std::string& id);
	void abort_process(const std::string& id);
	bool is_process_running(const std::string& id);
	void set_communication_command(const std::string& cmd) { _communication_command = cmd; }
protected:
	std::string write_read(const std::string& cmd);
	Tango::DeviceProxyHelper* _proxy;
	std::string _communication_command;
};

#endif // _INCLUDE_GALILBOXPROXY_H
