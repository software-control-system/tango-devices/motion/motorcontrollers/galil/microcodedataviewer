#include "GalilBoxProxy.h"

GalilBoxProxy::GalilBoxProxy() {
	_proxy = 0;
	_communication_command = "ExecLowLevelCmd";
}

GalilBoxProxy::~GalilBoxProxy() {
	if(_proxy) {
		delete _proxy;
		_proxy = 0;
	}
}

void GalilBoxProxy::init(string proxy_name,Tango::DeviceImpl* dev) {
	if( _proxy ) {
		delete _proxy;
		_proxy = 0;
	}
	_proxy = new Tango::DeviceProxyHelper(proxy_name,dev);
}

double GalilBoxProxy::read_var(const std::string& var_name) {
	std::string cmd = var_name + "=?";
	std::string res = write_read(cmd);
	double val = 0;
	std::istringstream ires(res);
	ires >> val;
	return val;
}

void GalilBoxProxy::write_var(const std::string& var_name,double value) {
	std::ostringstream ocmd;
	ocmd.precision(14);
	ocmd << var_name << "=" << value;
	string res = write_read(ocmd.str());
}

void GalilBoxProxy::start_process(const std::string& name,const std::string& id) {
	std::ostringstream ocmd;
	ocmd << "XQ" << name << "," << id;
	string res = write_read(ocmd.str());
}

void GalilBoxProxy::abort_process(const std::string& id) {
	std::ostringstream ocmd;
	ocmd << "HX" << id;
	string res = write_read(ocmd.str());
}

bool GalilBoxProxy::is_process_running(const std::string& id) {
	std::ostringstream ocmd;
	ocmd << "MG_XQ" << id;
	string res = write_read(ocmd.str());
	double val = 0;
	std::istringstream(res) >> val;
	return (val != -1);
}

std::string GalilBoxProxy::write_read(const std::string& cmd) {
	if(!_proxy) {
		Tango::Except::throw_exception (
			(const char *)"TANGO_DEVICE_ERROR",
			(const char *)"The GalilBoxProxy device proxy was not initialized !",
			(const char *)"GalilBoxProxy::write_read()");
	}
	std::string res;
	_proxy->command_inout(_communication_command,cmd,res);
	if(res[0]=='?') {
		ostringstream oerr;
		oerr << "The GalilBox returned an error to the command '" << cmd << "'";
		Tango::Except::throw_exception (
			(const char *)"TANGO_DEVICE_ERROR",
			(const char *)oerr.str().c_str(),
			(const char *)"GalilBoxProxy::write_read()");
	}
	return res;
}